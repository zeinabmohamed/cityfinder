/*
 * Copyright (c) 2017.  All Code CopyRights reserved For Zeinab Mohamed Abdelmawla
 */

package com.zm.org.cityfinder.view.findCity.fragment;


import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.View;

import com.zm.org.cityfinder.Navigator;
import com.zm.org.cityfinder.R;
import com.zm.org.cityfinder.model.findCity.model.City;
import com.zm.org.cityfinder.presenter.findCity.CityFinderPresenter;
import com.zm.org.cityfinder.view.HomeActivity;
import com.zm.org.cityfinder.view.base.activity.BaseActivity;
import com.zm.org.cityfinder.view.base.fragment.BaseLoadingFragment;
import com.zm.org.cityfinder.view.findCity.adapter.CityItemListener;
import com.zm.org.cityfinder.view.findCity.adapter.CityListAdapter;
import com.zm.org.cityfinder.view.findCity.listener.FindCityView;

import java.util.ArrayList;
import java.util.List;


public class CityFinderFragment extends BaseLoadingFragment<CityFinderPresenter> implements FindCityView, CityItemListener {


    private final List<City> CITIES = new ArrayList<>();
    private CityListAdapter cityListAdapter;
    private SearchView findCitySearchView;

    public CityFinderFragment() {
        presenter = new CityFinderPresenter(this);
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment CityFinderFragment.
     */
    public static CityFinderFragment newInstance() {
        return new CityFinderFragment();
    }

    @Override
    protected int getViewId() {
        return R.layout.fragment_city_finder;
    }

    @Override
    protected void onCreateView(View view) {
        findCitySearchView = view.findViewById(R.id.findCitySearchView);
        findCitySearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                cityListAdapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                cityListAdapter.getFilter().filter(newText);
                return false;
            }
        });
        RecyclerView cityListRecyclerView = view.findViewById(R.id.cityListRecyclerView);
        cityListRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        cityListAdapter = new CityListAdapter(this, CITIES);
        cityListRecyclerView.setAdapter(cityListAdapter);
        cityListRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(),
                DividerItemDecoration.VERTICAL));
    }

    @Override
    protected void onViewCreated(View view) {
        if (CITIES.isEmpty())
            presenter.getCities();
    }

    @Override
    public void showCities(List<City> response) {
        CITIES.clear();
        CITIES.addAll(response);
        cityListAdapter.notifyDataSetChanged();
    }


    @Override
    public void onCityItemClicked(City city) {
        Navigator.loadFragment((BaseActivity) getActivity(), CityLocationFragment.newInstance(city), HomeActivity.getContainerId(), true);
    }

    public SearchView getSearchView() {
        return findCitySearchView;
    }
}