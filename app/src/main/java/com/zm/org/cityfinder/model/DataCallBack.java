package com.zm.org.cityfinder.model;

/**
 * Created by zeinabmohamed on 11/15/17.
 */

/*
 * Copyright (c) 2017.  All Code CopyRights reserved For Zeinab Mohamed Abdelmawla
 *
 */

import com.zm.org.cityfinder.model.base.error.CityFinderError;


/**
 * Common response
 */
public interface DataCallBack<R> {

    void success(R response);
    void error(CityFinderError error);


}
