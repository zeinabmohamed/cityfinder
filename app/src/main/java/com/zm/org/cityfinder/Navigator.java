/*
 * Copyright (c) 2017.  All Code CopyRights reserved For Zeinab Mohamed Abdelmawla
 *
 */

package com.zm.org.cityfinder;

import android.view.WindowManager;

import com.zm.org.cityfinder.view.base.activity.BaseActivity;
import com.zm.org.cityfinder.view.base.fragment.BaseFragment;
import com.zm.org.cityfinder.view.splash.fragment.SplashFragment;


/**
 * Handle all navigation between screens from here
 */

public class Navigator {

    private Navigator() {

    }

    public static void loadFragment(BaseActivity activity,
                                    BaseFragment baseFragment, int containerId, boolean isStacked) {
        // make splash fragment full screen
        if (baseFragment instanceof SplashFragment) {
            activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            activity.getSupportActionBar().hide();
        } else {
            activity.getSupportActionBar().show();
        }
        if (!isStacked) {
            activity.getSupportFragmentManager()
                    .beginTransaction().replace(containerId,
                    baseFragment).commit();
        } else {
            activity.getSupportFragmentManager().beginTransaction().replace(containerId,
                    baseFragment).addToBackStack(baseFragment.getClass().getName()).commit();
        }
    }


}
