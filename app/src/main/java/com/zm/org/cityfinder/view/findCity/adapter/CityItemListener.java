package com.zm.org.cityfinder.view.findCity.adapter;

import com.zm.org.cityfinder.model.findCity.model.City;

/**
 * Created by zeinabmohamed on 1/9/18.
 */

public interface CityItemListener {
    void onCityItemClicked(City city);
}
