package com.zm.org.cityfinder.view.findCity.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.TextView;

import com.zm.org.cityfinder.R;
import com.zm.org.cityfinder.model.findCity.model.City;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zeinabmohamed on 1/8/18.
 */

public class CityListAdapter extends RecyclerView.Adapter<CityListAdapter.CityItemViewHolder> {
    private final List<City> cities;
    private final List<City> originalData;
    private CityItemListener cityItemListener;

    public CityListAdapter(CityItemListener cityItemListener, List<City> cities) {
        this.cityItemListener = cityItemListener;
        this.cities = cities;
        this.originalData = new ArrayList<>();
    }

    @Override
    public CityItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.city_list_item, parent, false);

        return new CityItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CityItemViewHolder holder, int position) {
        holder.item = cities.get(position);
        holder.bind();
    }

    @Override
    public int getItemCount() {
        return cities.size();
    }

    public Filter getFilter() {
        if (originalData.isEmpty()) {
            this.originalData.addAll(cities); // backup at original list
        }
        return new Filter() {

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                constraint = constraint.toString().toLowerCase();
                FilterResults result = new FilterResults();

                if (constraint != null && constraint.toString().length() > 0) {
                    List<City> founded = new ArrayList();
                    for (City item : originalData) {
                        if (item.name.toLowerCase().startsWith(String.valueOf(constraint))) {
                            founded.add(item);
                        }
                    }

                    result.values = founded;
                    result.count = founded.size();
                } else {
                    result.values = originalData;
                    result.count = originalData.size();
                }
                return result;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                cities.clear();
                for (City item : (List<City>) results.values) {
                    cities.add(item);
                }
                notifyDataSetChanged();

            }

        };
    }

    public class CityItemViewHolder extends RecyclerView.ViewHolder {

        private final TextView cityNameTextView;
        private City item;

        public CityItemViewHolder(View itemView) {
            super(itemView);
            cityNameTextView = itemView.findViewById(R.id.cityNameTextView);
        }

        public void bind() {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    cityItemListener.onCityItemClicked(item);
                }
            });
            cityNameTextView.setText(item.name + " , " + item.country);

        }
    }
}
