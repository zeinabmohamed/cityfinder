package com.zm.org.cityfinder.model.base;

import android.os.AsyncTask;

import com.google.gson.Gson;
import com.zm.org.cityfinder.AppApplication;
import com.zm.org.cityfinder.model.DataCallBack;
import com.zm.org.cityfinder.model.base.error.CityFinderError;
import com.zm.org.cityfinder.model.base.error.ErrorConstants;
import com.zm.org.cityfinder.model.base.error.ErrorUtil;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;

/**
 * Created by zeinabmohamed on 11/15/17.
 */

public abstract class BaseRepo<R> extends AsyncTask<Object, Void, Object> {
    protected DataCallBack<R> dataCallBack;
    // by default load data from cloud
    protected boolean isFromStubs = false;
    protected String jsonFileName = null;// to override it's value

    /**
     * params >> pass array of inputs as this sequence
     * isFromStubs , @Nullable jsonFileName  ,@NonNull {@link DataCallBack} + all next needed params
     *
     * @param params
     * @return
     */
    @Override
    protected Object doInBackground(Object... params) {

        dataCallBack = (DataCallBack) params[0];
        Object result = null;
        if (isFromStubs) {
            result = readFromAsset(jsonFileName);
        } else {
            // TODO: 1/8/18  get data from cloud and handle general network errors
        }

        if (result instanceof String) {  // success case
            Object response = parseJsonResult((String) result, getResponseClass());
            onBusinessHandling((R) response);
            return response;
        } else {
            return null;
        }
    }

    protected abstract void onBusinessHandling(R response);

    private Object parseJsonResult(String result, Type responseClass) {
        try {
            Gson gson = new Gson();
            return gson.fromJson(result, responseClass);
        } catch (Exception ex) {
            ex.printStackTrace();
            return ErrorUtil.generateError(ex, ErrorConstants.PARSING_ERROR);
        }
    }

    protected abstract Type getResponseClass();

    private Object readFromAsset(String jsonFileName) {
        try {
            InputStream is = AppApplication.getContext().getAssets().open(jsonFileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            return new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return ErrorUtil.generateError(ex, ErrorConstants.READ_FROM_ASSET_ERROR);
        }
    }

    @Override
    protected void onPostExecute(Object result) {
        super.onPostExecute(result);
        // handle data callback and general error handling 
        if (result != null) {
            if (result instanceof CityFinderError) {
                dataCallBack.error((CityFinderError) result);
            } else {
                dataCallBack.success((R) result);
            }
        } else {
            dataCallBack.error(ErrorUtil.generateError(ErrorConstants.GENERAL_ERROR));
        }
    }
}
