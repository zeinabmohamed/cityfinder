/*
 * Copyright (c) 2017.  All Code CopyRights reserved For Zeinab Mohamed Abdelmawla
 *
 */

package com.zm.org.cityfinder.view;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.zm.org.cityfinder.Navigator;
import com.zm.org.cityfinder.R;
import com.zm.org.cityfinder.view.base.activity.BaseActivity;
import com.zm.org.cityfinder.view.splash.fragment.SplashFragment;

public class HomeActivity extends BaseActivity {

    private static int containerId = R.id.home_container;

    public static int getContainerId() {
        return containerId;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Navigator.loadFragment(this, SplashFragment.newInstance(), containerId, false);
    }

}
