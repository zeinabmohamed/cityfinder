package com.zm.org.cityfinder.model.findCity.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by zeinabmohamed on 1/8/18.
 */

public class Location implements Serializable {
    /**
     * lon : 34.283333
     * lat : 44.549999
     */

    @SerializedName("lon")
    public double lon;
    @SerializedName("lat")
    public double lat;
}
