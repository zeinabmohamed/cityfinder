/*
 * Copyright (c) 2017.  All Code CopyRights reserved For Zeinab Mohamed Abdelmawla
 *
 */

package com.zm.org.cityfinder.view.base.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.Toast;

import com.zm.org.cityfinder.R;
import com.zm.org.cityfinder.presenter.base.BasePresenter;
import com.zm.org.cityfinder.view.base.listener.LoadDataView;


/**
 * BaseLoadingFragment for all common handling that needed to request data from model
 */
public abstract class BaseLoadingFragment<P extends BasePresenter> extends BaseFragment implements LoadDataView {


    protected P presenter;
    private ProgressDialog progressLoading;

    protected P getPresenter() {
        return presenter;
    }
    @Override
    public Context context() {
        return getActivity();
    }

    @Override
    public void showLoading() {
        progressLoading = new ProgressDialog(getActivity());
        progressLoading.setCancelable(false);
        progressLoading.show();
    }

    @Override
    public void hideLoading() {
        if (progressLoading != null & progressLoading.isShowing()) {
            progressLoading.dismiss();
        }
    }

    @Override
    public void showRetry(String message) {
        Snackbar mySnackbar = Snackbar.make(getView(),
                message, Snackbar.LENGTH_SHORT);
        mySnackbar.setAction(R.string.retry, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.loadData();
            }
        });
        mySnackbar.show();
    }

    @Override
    public void hideRetry() {
// not needed for now
    }

    @Override
    public void showError(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }
}
