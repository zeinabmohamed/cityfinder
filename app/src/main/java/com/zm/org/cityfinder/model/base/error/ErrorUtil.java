package com.zm.org.cityfinder.model.base.error;

/**
 * Created by zeinabmohamed on 1/8/18.
 */

public class ErrorUtil {
    public static CityFinderError generateError(Exception ex, int errorType) {
        CityFinderError error = new CityFinderError(ex, errorType);

        return error;
    }

    public static CityFinderError generateError(int errorType) {
        CityFinderError error = new CityFinderError(errorType);

        return error;
    }
}
