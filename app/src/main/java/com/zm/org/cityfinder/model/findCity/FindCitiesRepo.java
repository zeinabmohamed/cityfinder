package com.zm.org.cityfinder.model.findCity;

import com.google.gson.reflect.TypeToken;
import com.zm.org.cityfinder.AppConstant;
import com.zm.org.cityfinder.model.DataCallBack;
import com.zm.org.cityfinder.model.base.BaseRepo;
import com.zm.org.cityfinder.model.findCity.model.City;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by zeinabmohamed on 01/07/17.
 */

public class FindCitiesRepo extends BaseRepo<List<City>> {

    public FindCitiesRepo() {
        isFromStubs = true;
        jsonFileName = AppConstant.GET_CITIES_STUB;
    }

    public void getCities(DataCallBack<List<City>>... dataCallBack) {
        execute(dataCallBack);
    }

    @Override
    protected void onBusinessHandling(List<City> response) {
        // sort alphabetically
        Collections.sort(response);
    }

    @Override
    protected Type getResponseClass() {
        return new TypeToken<ArrayList<City>>() {
        }.getType();
    }


}
