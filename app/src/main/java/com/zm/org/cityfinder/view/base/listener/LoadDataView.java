/*
 * Copyright (c) 2017.  All Code CopyRights reserved For Zeinab Mohamed Abdelmawla
 *
 */

package com.zm.org.cityfinder.view.base.listener;

import android.content.Context;

/**
 * Interface representing a View that will use to load data.
 * adding default handling for methods in {@link com.zm.org.cityfinder.view.base.fragment.BaseLoadingFragment}
 */
public interface LoadDataView {


    /**
     * Show a view with a progress bar indicating a loading process.
     */
    void showLoading();

    /**
     * Hide a loading view.
     */
    void hideLoading();

    /**
     * Show a retry view in case of an error when retrieving data.
     *
     * @param message
     */
    void showRetry(String message);

    /**
     * Hide a retry view shown if there was an error when retrieving data.
     */
    void hideRetry();

    /**
     * Show an error message
     *
     * @param message A string representing an error.
     */
    void showError(String message);

    /**
     * Get a [Context].
     */
    Context context();

}
