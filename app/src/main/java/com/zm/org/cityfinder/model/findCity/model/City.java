package com.zm.org.cityfinder.model.findCity.model;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by zeinabmohamed on 1/8/18.
 */

public class City implements Comparable<City>, Serializable {

    /**
     * country : UA
     * name : Hurzuf
     * _id : 707860
     * coord : {"lon":34.283333,"lat":44.549999}
     */

    @SerializedName("country")
    public String country;
    @SerializedName("name")
    public String name;
    @SerializedName("_id")
    public int id;
    @SerializedName("coord")
    public Location coord;

    @Override
    public int compareTo(@NonNull City city) {
        if (city.name.toLowerCase().trim().compareTo(name
                .toLowerCase().trim()) < 0) { // next city name lower than this
            return 1;
        } else if (city.name.toLowerCase().trim().compareTo(name
                .toLowerCase().trim()) > 0) {
            return -1;
        } else {
            if (city.country.toLowerCase().compareTo(country.toLowerCase()) < 0) {
                return 1;
            } else if (city.country.toLowerCase().compareTo(country.toLowerCase()) > 0) {
                return -1;
            } else {
                return 0;
            }
        }
    }
}
