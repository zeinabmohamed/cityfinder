package com.zm.org.cityfinder.view.findCity.listener;


import com.zm.org.cityfinder.model.findCity.model.City;
import com.zm.org.cityfinder.view.base.listener.LoadDataView;

import java.util.List;

/**
 * Created by zeinabmohamed on 11/15/17.
 */

public interface FindCityView extends LoadDataView {
    void showCities(List<City> response);
}
