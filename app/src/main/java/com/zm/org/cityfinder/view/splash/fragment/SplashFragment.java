/*
 * Copyright (c) 2017.  All Code CopyRights reserved For Zeinab Mohamed Abdelmawla
 *
 */

package com.zm.org.cityfinder.view.splash.fragment;


import android.os.Handler;
import android.view.View;

import com.zm.org.cityfinder.Navigator;
import com.zm.org.cityfinder.R;
import com.zm.org.cityfinder.view.HomeActivity;
import com.zm.org.cityfinder.view.base.activity.BaseActivity;
import com.zm.org.cityfinder.view.base.fragment.BaseFragment;
import com.zm.org.cityfinder.view.findCity.fragment.CityFinderFragment;


/**
 * A simple [Fragment] subclass.
 * Use the [SplashFragment.newInstance] factory method to
 * create an instance of this fragment.
 */

/**
 * Splash screen is the app entry point
 */
public class SplashFragment extends BaseFragment {
    /**
     * default splash delay time  sec
     */
    private static final long SPLASH_SCREEN_DELAY_TIME = 5000;


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment SplashFragment.
     */
    public static SplashFragment newInstance() {
        return new SplashFragment();
    }

    @Override
    protected int getViewId() {
        return R.layout.fragment_splash;
    }

    @Override
    protected void onCreateView(View view) {
    }

    @Override
    protected void onViewCreated(View view) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Navigator.loadFragment((BaseActivity) getActivity(), CityFinderFragment.newInstance(), HomeActivity.getContainerId(), false);
            }
        }, SPLASH_SCREEN_DELAY_TIME);
    }


}

