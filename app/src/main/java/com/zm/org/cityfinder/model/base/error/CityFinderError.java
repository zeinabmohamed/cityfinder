package com.zm.org.cityfinder.model.base.error;

import com.zm.org.cityfinder.AppApplication;
import com.zm.org.cityfinder.R;

/**
 * Created by zeinabmohamed on 1/8/18.
 */

public class CityFinderError extends Error {
    public final int errorType;
    private Exception ex;
    private String errorMessage;

    public CityFinderError(Exception ex, int errorType) {

        this.ex = ex;
        this.errorType = errorType;
    }

    public CityFinderError(int errorType) {
        this.errorType = errorType;
    }

    public String getErrorMessage() {
        switch (errorType) {
            case ErrorConstants.PARSING_ERROR:
                errorMessage = AppApplication.getContext().getString(R.string.general_error);
                break;
            case ErrorConstants.READ_FROM_ASSET_ERROR:
                errorMessage = AppApplication.getContext().getString(R.string.general_error);
                break;
            default:
                errorMessage = AppApplication.getContext().getString(R.string.general_error);
                break;
        }
        return errorMessage;
    }
}
