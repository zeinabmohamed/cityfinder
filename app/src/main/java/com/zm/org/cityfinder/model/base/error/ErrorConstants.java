package com.zm.org.cityfinder.model.base.error;

/**
 * Created by zeinabmohamed on 1/8/18.
 */

public class ErrorConstants {
    public static final int PARSING_ERROR = 1000;
    public static final int READ_FROM_ASSET_ERROR = 1001;
    public static final int GENERAL_ERROR = 1002;


}
