/*
 * Copyright (c) 2017.  All Code CopyRights reserved For Zeinab Mohamed Abdelmawla
 *
 */

package com.zm.org.cityfinder.presenter.base;


import com.zm.org.cityfinder.model.DataCallBack;
import com.zm.org.cityfinder.model.base.error.CityFinderError;
import com.zm.org.cityfinder.view.base.listener.LoadDataView;

/**
 */
public abstract class BasePresenter<T extends LoadDataView, R> implements DataCallBack<R> {

    private T view;

    public BasePresenter(T view) {
        this.view = view;
    }

    /**
     * return attached view to the presenter
     *
     * @return
     */
    public T getView() {
        return view;
    }

    /**
     * to reload data in case of retry
     */
    public abstract void loadData();

    /**
     * handle common  error for custom handling override method in child presenter
     *
     * @param error
     */
    @Override
    public void error(CityFinderError error) {
        // handle rest of cases
        switch (error.errorType) {
            default:
                getView().hideLoading();
                getView().showRetry(error.getErrorMessage());
                break;
        }
    }
}
