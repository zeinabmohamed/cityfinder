package com.zm.org.cityfinder.presenter.findCity;

import com.zm.org.cityfinder.model.findCity.FindCitiesRepo;
import com.zm.org.cityfinder.model.findCity.model.City;
import com.zm.org.cityfinder.presenter.base.BasePresenter;
import com.zm.org.cityfinder.view.findCity.listener.FindCityView;

import java.util.List;


/**
 * Created by zeinabmohamed on 11/15/17.
 */

public class CityFinderPresenter extends BasePresenter<FindCityView, List<City>> {

    public CityFinderPresenter(FindCityView view) {
        super(view);
    }

    @Override
    public void loadData() {
        getView().showLoading();
        new FindCitiesRepo().getCities(this);
    }

    /**
     * load the cities from model layer and pass if any required params from ui
     */

    public void getCities() {
        loadData();
    }

    @Override
    public void success(List<City> response) {
        getView().showCities(response);
        getView().hideLoading();
    }

}
