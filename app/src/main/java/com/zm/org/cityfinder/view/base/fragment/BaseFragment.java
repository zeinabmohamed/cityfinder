/*
 * Copyright (c) 2017.  All Code CopyRights reserved For Zeinab Mohamed Abdelmawla
 *
 */

package com.zm.org.cityfinder.view.base.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * BaseFragment for all common handling
 */
public abstract class BaseFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(getViewId(), container, false);
        onCreateView(view);
        return view;
    }

    /**
     * get layout resource id to inflate it in common point
     *
     * @return
     */
    protected abstract int getViewId();

    /**
     * for initialise view from common point
     *
     * @param view
     */
    protected abstract void onCreateView(View view);

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        onViewCreated(view);
    }

    /**
     * to handle what after view created from common point
     *
     * @param view
     */
    protected abstract void onViewCreated(View view);

}
