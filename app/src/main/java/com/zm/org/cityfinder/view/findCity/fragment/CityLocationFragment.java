/*
 * Copyright (c) 2017.  All Code CopyRights reserved For Zeinab Mohamed Abdelmawla
 */

package com.zm.org.cityfinder.view.findCity.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.zm.org.cityfinder.R;
import com.zm.org.cityfinder.model.findCity.model.City;
import com.zm.org.cityfinder.view.base.fragment.BaseFragment;


public class CityLocationFragment extends BaseFragment implements OnMapReadyCallback {


    private static final String CITY_ITEM = "city_item";
    private City city;

    public CityLocationFragment() {
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param city
     * @return A new instance of fragment CityLocationFragment.
     */
    public static CityLocationFragment newInstance(City city) {

        Bundle args = new Bundle();
        args.putSerializable(CITY_ITEM, city);
        CityLocationFragment fragment = new CityLocationFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getViewId() {
        return R.layout.fragment_city_location;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            city = (City) getArguments().getSerializable(CITY_ITEM);
        }
    }

    @Override
    protected void onCreateView(View view) {
        // Get the SupportMapFragment and request notification
        // when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    protected void onViewCreated(View view) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (city != null) {
            LatLng cityLatLng = new LatLng(city.coord.lat, city.coord.lon);
            googleMap.addMarker(new MarkerOptions().position(cityLatLng)
                    .title(city.name + " , " + city.country));
            googleMap.moveCamera(CameraUpdateFactory.newLatLng(cityLatLng));
        }
    }
}