package com.zm.org.cityfinder;

import android.content.Context;
import android.support.annotation.IdRes;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.zm.org.cityfinder.view.HomeActivity;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withHint;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static junit.framework.Assert.assertFalse;
import static junit.framework.TestCase.assertEquals;
import static org.hamcrest.Matchers.allOf;


/**
 * Instrumented test, which will execute on an Android device.
 * <p>
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4.class)
public class CityFinderFragmentTest {
    @Rule
    public ActivityTestRule<HomeActivity> menuActivityTestRule = new ActivityTestRule<>(HomeActivity.class);

    private static int getCountFromRecyclerView(@IdRes int RecyclerViewId) {
        final int[] COUNT = {0};
        Matcher matcher = new TypeSafeMatcher<View>() {
            @Override
            protected boolean matchesSafely(View item) {
                COUNT[0] = ((RecyclerView) item).getAdapter().getItemCount();
                return true;
            }

            @Override
            public void describeTo(Description description) {
            }
        };
        onView(allOf(withId(RecyclerViewId), isDisplayed())).check(matches(matcher));
        int result = COUNT[0];
        return result;
    }

    @Test
    public void useAppContext() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();
        assertEquals("com.zm.org.citiyfinder", appContext.getPackageName());
    }

    @Test
    public void searchCaseWithQuery_Klam_isCorrect() {
        // splash delay
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // load data delay
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // the result of that Query 7 cities and will assert that value
        onView(withHint("Find City")).perform(typeText("Klam"));
        //ViewInteraction recyclerView = onView(allOf(withId(R.id.cityListRecyclerView), isDisplayed()));

        // wait until notify recyclerView
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        assertEquals(7, getCountFromRecyclerView(R.id.cityListRecyclerView));

        //recyclerView.perform(actionOnItemAtPosition(4, click()));

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void searchCaseWithQuery_ncm_isFalse() {
        // splash delay
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // load data delay
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // the result of that Query 7 cities and will assert that value
        onView(withHint("Find City")).perform(typeText("ncm"));
        //ViewInteraction recyclerView = onView(allOf(withId(R.id.cityListRecyclerView), isDisplayed()));

        // wait until notify recyclerView
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        assertFalse("Search result  is empty !", (getCountFromRecyclerView(R.id.cityListRecyclerView) == 1000));

        //recyclerView.perform(actionOnItemAtPosition(4, click()));

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
