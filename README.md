
Android City Finder Mobile App|
===================================

This application sample of City Finder with basic functionality (view search for city and their location ) .

Programing Language: 
- Android 

Pre-requisites
--------------

- Android SDK 27
- Android Build Tools v27.0.2

Screenshots
-------------
![splash](screenshots/splash.png)
![cities_list](screenshots/cities_list.png)
![search_city](screenshots/search_city.png)
![city_details](screenshots/city_details.png)

Getting Started
---------------

This App uses the Gradle build system. To build this project, use the
"gradlew build" command or use "Import Project" in Android Studio.

Dependencies
-------
- 'com.android.support:appcompat-v7:27.0.2'
- 'junit:junit:4.12'
- 'com.android.support:design:27.0.2'
- 'com.android.support:support-annotations:27.0.2'
- 'com.android.support.test:runner:1.0.1'
- 'com.android.support.test.espresso:espresso-core:3.0.1'
- 'com.android.support.test.espresso:espresso-contrib:2.2.2'
- 'com.google.code.gson:gson:2.8.1'
- 'com.google.android.gms:play-services-maps:11.8.0'

License
-------

Copyright (c) 2017.  All Code CopyRights reserved For Zeinab Mohamed Abdelmawla
